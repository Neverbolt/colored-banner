import json
import random
from collections import defaultdict
from typing import Any, List, Dict

import jinja2
import asyncio
import aiohttp_jinja2
from aiohttp import web, WSMsgType
from dataclasses import dataclass, field


@dataclass
class Player:
    ws: web.WebSocketResponse
    red_cards: List[str] = field(default_factory=list)
    placed_red: List[str] = field(default_factory=list)
    white_cards: List[str] = field(default_factory=list)
    placed_white: List[str] = field(default_factory=list)
    provided_red_cards: List[str] = field(default_factory=list)
    provided_white_cards: List[str] = field(default_factory=list)
    points: int = 0
    connected: bool = True


class GameRoom:
    def __init__(self, max_players=8, max_red_cards=3, max_white_cards=6, max_placed_red=1, max_placed_white=2, cleanup_interval=60):
        self.players: Dict[str, Player] = {}
        self.white_cards = []
        self.max_placed_white = max_placed_white
        self.red_cards = []
        self.max_placed_red = max_placed_red
        self.max_players = max_players
        self.max_red_cards = max_red_cards
        self.max_white_cards = max_white_cards
        self.upcomingPlayers = []
        self.czar = ""

        self.cleanup_interval = cleanup_interval
        self.cleanup_task = None

        self.state = "joining"

    async def cleanup(self):
        while True:
            await asyncio.sleep(self.cleanup_interval)
            disconnected_players = [player_name for player_name, player in self.players.items() if not player.connected]
            if len(disconnected_players) == 0:
                continue
            print("clean", disconnected_players)
            for player in disconnected_players:
                await self.remove_player(player)

    async def __aenter__(self):
        self.cleanup_task = asyncio.create_task(self.cleanup())
        return self

    async def __aexit__(self, exc_type, exc, tb):
        self.cleanup_task.cancel()

    async def provide_cards(self, player_name, cards):
        if player_name not in self.players or self.state != "joining":
            print("provide_cards: player non existent, or wrong state", self.state)
            return

        player = self.players[player_name]
        player.provided_red_cards = cards["red"]
        player.provided_white_cards = cards["white"]

    async def play_card(self, player_name, content):
        print("playing card", player_name, content, self.state)
        if player_name not in self.players \
                or player_name == self.czar \
                or self.state not in ("placing_cards_white", "placing_cards_red"):
            print("play_card: player non existent, czar, or wrong state", self.state)
            return

        player = self.players[player_name]
        if self.state == "placing_cards_white":
            if content not in player.white_cards:
                print("play_card: content not in player white cards", player_name, content)
                return
            if len(player.placed_white) >= self.max_placed_white:
                print("play_card: player already played all white cards", player_name, content)
                return
            player.placed_white.append(content)
            player.white_cards.remove(content)
            print("play_card: played card", player_name, content)
            await self.broadcast({"action": "play_card", "payload": {"player_name": player_name, "content": content, "type": "white"}})
            if all(len(player.placed_white) >= self.max_placed_white or player_name == self.czar for player_name, player in self.players.items()):
                print("play_card: all white cards played")
                self.state = "revealing_cards_white"

        elif self.state == "placing_cards_red":
            if content not in player.red_cards:
                print("play_card: content not in player red cards", player_name, content)
                return
            red_target_name = self.get_red_card_target(player_name)
            red_target = self.players[red_target_name]
            if len(red_target.placed_red) >= self.max_placed_red:
                print("play_card: player already played all red cards", player_name, content)
                return
            red_target.placed_red.append(content)
            player.red_cards.remove(content)
            await self.broadcast({"action": "play_card", "payload": {"player_name": red_target_name, "content": content, "type": "red"}})
            await player.ws.send_json({"action": "remove_card_from_deck", "payload": {"content": content}})
            if all(len(player.placed_red) >= self.max_placed_red or player_name == self.czar for player_name, player in self.players.items()):
                print("play_card: all red cards played")
                self.state = "revealing_cards_red"
        else:
            print("something is wrong", self.state)

    def get_red_card_target(self, player_name):
        players = list(self.players.keys())
        players.remove(self.czar)
        players += players
        return players[players.index(player_name) + 1]

    async def show_card(self, player_name, card_player_name, content, typ):
        if player_name not in self.players \
                or player_name != self.czar \
                or card_player_name not in self.players \
                or self.state not in ("revealing_cards_white", "revealing_cards_red"):
            print("show_card: player non existent, or wrong state", self.state)
            return

        player = self.players[card_player_name]
        if typ == "white":
            if content not in player.placed_white:
                print("show_card: content not in player white cards", player_name, content)
                return
            player.placed_white.remove(content)
            if all(len(player.placed_white) == 0 for player in self.players.values()):
                print("show_card: all white cards revealed")
                self.state = "placing_cards_red"
        elif typ == "red":
            if content not in player.placed_red:
                print("show_card: content not in player red cards", player_name, content)
                return
            player.placed_red.remove(content)
            if all(len(player.placed_red) == 0 for player in self.players.values()):
                print("show_card: all red cards revealed")
                self.state = "choosing_winner"
        else:
            return

        await self.broadcast({"action": "show_card", "payload": {"player_name": card_player_name, "content": content, "type": typ}})

    async def select_winner(self, player_name, winner_name):
        if player_name not in self.players \
                or player_name != self.czar \
                or winner_name not in self.players \
                or self.state != "choosing_winner":
            print("select_winner: player non existent, or wrong state", self.state)
            return
        if winner_name == self.czar:
            print("select_winner: czar can't win")
            return
        winner = self.players[winner_name]
        winner.points += 1
        await self.broadcast({"action": "selected_winner", "payload": {"player_name": winner_name, "points": winner.points}})
        if len(self.upcomingPlayers) == 0:
            print("select_winner: game over")
            self.upcomingPlayers = list(self.players.keys())
        print("select_winner: serving cards")
        self.state = "serving"
        self.czar = self.upcomingPlayers.pop()
        await self.broadcast({"action": "set_czar", "payload": {"player_name": self.czar}})
        await self.serve_cards()

    async def add_player(self, player_ws, player_name):
        re_joined = player_name in self.players
        if not re_joined and self.state != "joining":
            print("add_player: game already started", self.state)
            return False

        if len(self.players) < self.max_players:
            for other_player_name in self.players:
                await player_ws.send_json({"action": "player_joined", "payload": {"player_name": other_player_name}})

            if re_joined:
                print("player re-joined", player_name)
                player = self.players[player_name]
                player.ws = player_ws
                player.connected = True
                await player_ws.send_json({"action": "re_enter_cards", "payload": {
                    "white": player.provided_white_cards,
                    "red": player.provided_red_cards,
                }})
            else:
                print("player joined new", player_name)
                self.players[player_name] = Player(player_ws)
                await self.broadcast({"action": "player_joined", "payload": {"player_name": player_name}})

            return True
        print("add_player: game full", self.state)
        return False

    async def serve_cards(self):
        if self.state != "serving":
            print("serve_cards: wrong state", self.state)
            return

        served_card = True
        while served_card:
            served_card = False
            for player_name, player in self.players.items():
                new_cards = {"white": [], "red": []}
                if self.max_red_cards > len(player.red_cards) and len(self.red_cards) > 0:
                    card = self.red_cards.pop()
                    new_cards["red"].append(card)
                    player.red_cards.append(card)
                    served_card = True
                if self.max_white_cards > len(player.white_cards) and len(self.white_cards) > 0:
                    card = self.white_cards.pop()
                    new_cards["white"].append(card)
                    player.white_cards.append(card)
                    served_card = True
                await player.ws.send_json({"action": "draw_cards", "payload": new_cards})
        print("serve_cards: serving cards done")
        self.state = "placing_cards_white"

    async def start_game(self):
        if self.state != "joining":
            print("start_game: wrong state", self.state)
            return
        if len(self.players) <= 2:
            print("start_game: not enough players")
            return
        self.red_cards = [c for p in self.players.values() for c in p.provided_red_cards]
        random.shuffle(self.red_cards)
        self.white_cards = [c for p in self.players.values() for c in p.provided_white_cards]
        random.shuffle(self.white_cards)
        self.upcomingPlayers = list(self.players.keys())
        self.czar = self.upcomingPlayers.pop()
        await self.broadcast({"action": "set_czar", "payload": {"player_name": self.czar}})
        await self.broadcast({"action": "start_game"})
        self.state = "serving"
        await self.serve_cards()

    def set_disconnected(self, player_name):
        print("setting player disconnected", player_name)
        self.players[player_name].connected = False

    async def remove_player(self, player_name):
        print("removing player", player_name)
        del self.players[player_name]
        await self.broadcast({"action": "remove_player", "payload": {"player_name": player_name}})

    async def broadcast(self, message):
        for player in self.players.values():
            await player.ws.send_json(message)

    # Add game logic and state machine functions here


rooms = defaultdict(GameRoom)


async def handle_index(request):
    return aiohttp_jinja2.render_template("index.html", request, {})


async def handle_websocket(request):
    room_id = request.match_info["room_id"]
    player_name = request.match_info["player_name"]

    async with rooms[room_id] as room:
        ws = web.WebSocketResponse()
        await ws.prepare(request)

        joined = await room.add_player(ws, player_name)
        if not joined:
            await ws.send_json({"action": "error", "message": "Room is full."})
            return ws

        try:
            print("websocket connected for room", room_id, "with player name", player_name)
            async for msg in ws:
                if msg.type == WSMsgType.TEXT:
                    data = json.loads(msg.data)
                    print(data)
                    match data["action"]:
                        case "start_game":
                            await room.start_game()
                        case "provide_cards":
                            await room.provide_cards(player_name, data["payload"])
                        case "play_card":
                            await room.play_card(player_name, data["payload"]["content"])
                        case "show_card_red":
                            await room.show_card(player_name, data["payload"]["player_name"], data["payload"]["content"], "red")
                        case "show_card_white":
                            await room.show_card(player_name, data["payload"]["player_name"], data["payload"]["content"], "white")
                        case "select_winner":
                            await room.select_winner(player_name, data["payload"]["player_name"])
                        case _:
                            print("unknown action", data["action"])

        finally:
            room.set_disconnected(player_name)

        return ws


app = web.Application()
aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader("templates"))
app.router.add_get("/", handle_index)
app.router.add_get("/ws/{room_id}/{player_name}", handle_websocket)

if __name__ == "__main__":
    web.run_app(app)
